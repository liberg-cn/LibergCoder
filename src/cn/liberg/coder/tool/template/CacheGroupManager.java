package cn.liberg.coder.tool.template;

import cn.liberg.coder.tool.LibergToolException;

import java.util.*;

public class CacheGroupManager {
    Map<String, Integer> map = new HashMap<>();
    List<CacheGroup> list = new ArrayList<>();

    public void addField(String groupName, CacheField field) {
        Integer idx = map.get(groupName);
        if(idx == null) {
            map.put(groupName, list.size());
            list.add(new CacheGroup(field));
        } else {
            list.get(idx).addField(field);
        }
    }

    public List<CacheGroup> getGroups() throws LibergToolException {
        for(CacheGroup group : list) {
            //组内根据seq值，由小到大排序
            group.build();
        }
        return list;
    }
}
