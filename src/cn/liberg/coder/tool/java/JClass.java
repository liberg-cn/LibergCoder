package cn.liberg.coder.tool.java;

import cn.liberg.coder.tool.LibergToolException;
import cn.liberg.coder.tool.core.Formats;
import cn.liberg.coder.tool.util.FileUtils;
import cn.liberg.coder.tool.util.RegExpr;
import cn.liberg.coder.tool.core.ILineReader;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;

/**
 * Java文件(.java)解析器
 * 1.暂不支持静态内部类
 * 2.所有成员必须以public/private/protect访问修饰符打头
 */
public class JClass implements ILineReader {
	public String mCodeFilePath;
	public boolean loadedFromFile;
	public List<JDesc> fileDescs;//文件最开始的注释
	public String mPackage;
	private LinkedHashSet<String> mImports;

	public String name;
	public List<JDesc> classDescs;//类定义开始的注释
	public List<String> classAnnos;//class注解
	public MetaAnno metaAnno;//类的注解信息


	public String defLine;//class定义的一行
	public String visitor = "public";//目前仅支持单个java文件内的顶级public类
	public boolean isStatic;
	public boolean isFinal;

	// 方法名作为key，不支持方法重载
	protected LinkedHashMap<String, JMethod> methods;
	protected LinkedHashMap<String, JField> fields;
	private BufferedReader mBr = null;
	public static final RegExpr RE = new RegExpr("^public +(static +)?(final +)?class +(\\w+)");

	public JClass(String path) throws LibergToolException {
		this(path, true);
	}

	public JClass(String path, boolean autoLoad) throws LibergToolException {
		mCodeFilePath = path;
		File file = new File(path);
		fileDescs = new ArrayList<>();
		mImports = new LinkedHashSet<>();
		classDescs = new ArrayList<>();
		classAnnos = new ArrayList<>();
		fields = new LinkedHashMap<>();
		methods = new LinkedHashMap<>();
		if(autoLoad && file.exists()) {
			loadedFromFile = true;
			try {
				mBr = FileUtils.bufferedReader(file);
				tryParse();
			} catch (Exception e) {
				throw new LibergToolException("Parse error: " + e.getMessage());
			} finally {
				if(mBr != null) {
					try {
						mBr.close();
					} catch (IOException e) {
					}
				}
			}
		}
	}

	public LinkedHashMap<String, JMethod> getMethods() {
		return methods;
	}

	public LinkedHashMap<String, JField> getFields() {
		return fields;
	}

	public MetaAnno getMetaAnno() {
		if(metaAnno == null && classAnnos.size() > 0) {
			metaAnno = new MetaAnno(classAnnos);
		}
		return metaAnno;
	}

	public void removeFieldsStartsWith(String prefix) {
		Iterator<Map.Entry<String, JField>> iter = fields.entrySet().iterator();
		while(iter.hasNext()) {
			Map.Entry<String, JField> entry = iter.next();
			String methodName = entry.getKey();
			if(methodName.startsWith(prefix)) {
				iter.remove();
			}
		}
	}

	public void removeFieldsByType(String type) {
		JField jf;
		Iterator<Map.Entry<String, JField>> iter = fields.entrySet().iterator();
		while(iter.hasNext()) {
			Map.Entry<String, JField> entry = iter.next();
			jf = entry.getValue();
			if(type.equals(jf.type)) {
				iter.remove();
			}
		}
	}

	public void addMethod(JMethod method) {
		methods.put(method.name, method);
	}

	public void addField(JField field) {
		fields.put(field.name, field);
	}
	public void addFieldIfAbsent(JField field) {
		if(!fields.containsKey(field.name)) {
			fields.put(field.name, field);
		}
	}

	public void updateMethod(JMethod method) {
		String methodName = method.name;
		JMethod jm = methods.get(methodName);
		if(jm != null) {
			methods.put(methodName, method);
		}
	}

	public boolean addOrUpdateMethod(JMethod method) {
		JMethod jm = methods.remove(method.name);
		if(jm != null) {
			method = jm;
		}
		addMethod(method);
		return jm == null;
	}

	public boolean addOrUpdateMethodSignature(JMethod method) {
		// 先删除，再添加，保持方法的书写顺序
		JMethod jm = methods.remove(method.name);
		if(jm != null) {
			// 更新方法签名，返回值类型等
			jm.setArgList(method.getArgList());
			jm.returnType = method.returnType;
			jm.restPart = method.restPart;
			if(jm.descTop == null) {
				jm.descTop = method.descTop;
			}
			method = jm;
		}
		addMethod(method);
		return jm == null;
	}

	public JMethod getMethod(String name) {
		return methods.get(name);
	}
	public boolean hasMethod(String name) {
		return methods.containsKey(name);
	}

	public boolean addMethodIfAbsent(JMethod method) {
		JMethod jm = methods.get(method.name);
		if(jm == null) {
			addMethod(method);
			return true;
		} else {
			return false;
		}
	}

	public boolean addOrUpdateField(JField field) {
		String fieldName = field.name;
		JField jf = fields.get(fieldName);
		if(jf != null) {
			fields.put(fieldName, field);
			return false;
		}
		addField(field);
		return true;
	}
	
	public void appendLineInMethod(String methodName, String line) {
		JMethod jm = methods.get(methodName);
		if(jm != null) {
			jm.appendBodyLine(line);
		}
	}

	public void addImport(String importClass) {
		if(!mImports.contains(importClass)) {
			mImports.add(importClass);
		}
	}

	public void writeToFile(String path) throws LibergToolException {
		BufferedWriter bw = null;
		try {
			File file = new File(path);
			if(!file.exists()) {
				file.createNewFile();
			}
			bw = FileUtils.bufferedWriter(file);
			for(JDesc it : fileDescs) {
				it.wirteTo(bw);
			}
			bw.write("package " + mPackage + ";");
			bw.write(Formats.NL2);
			for(String it : mImports) {
				bw.write("import ");
				bw.write(it);
				bw.write(";");
				bw.write(Formats.NL);
			}
			bw.write(Formats.NL);
			for(JDesc it : classDescs) {
				it.wirteTo(bw);
			}
			for(String it : classAnnos) {
				bw.write(it);
				bw.write(Formats.NL);
			}
			bw.write(defLine);
			bw.write(Formats.NL);

			writeFields(bw);
			bw.write(Formats.NL);
			writeMethods(bw);
			bw.write("}");
		} catch (IOException e) {
			e.printStackTrace();
			throw new LibergToolException(e);
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	protected void writeFields(BufferedWriter bw) throws IOException {
		for (Map.Entry<String, JField> entry : fields.entrySet()) {
			entry.getValue().writeTo(bw);
		}
	}

	protected void writeMethods(BufferedWriter bw) throws IOException {
		for (Map.Entry<String, JMethod> entry : methods.entrySet()) {
			entry.getValue().writeTo(bw);
		}
	}

	public static void main(String[] args) throws LibergToolException {
		JClass jClass = new JClass("D:\\dev\\java\\work\\databoard\\src\\main\\java\\cn\\jxm\\databoard\\service\\ReceiverService.java");
		System.out.println(1);
		System.out.println(2);
		jClass.writeToFile("D:/test/test123.java");
	}

	private void tryParse() throws Exception {
		String line;
		
		//parse head
		int step = 0;
		while((line = next()) != null) {
			if(line.startsWith("//") || line.startsWith("/*")) {
				if(step == 0) {
					fileDescs.add(JDesc.parse(this, line));
				} else {
					classDescs.add(JDesc.parse(this, line));
				}
			} else if(line.startsWith("package")) {
				step = 1;
				mPackage = line.substring(8, line.length()-1).trim();
			} else if(line.startsWith("import")) {
				step = 1;
				addImport(line.substring(7, line.length()-1).trim());
			} else if(line.startsWith("@")) {//类的注解开始
				step = 1;
				classAnnos.add(line);
			} else if(line.indexOf(" class ")>=0) {
				defLine = line;
				parseDefLine(line);
				break;
			}
		}
		//parse body
		JDesc topDesc = null;
		boolean isInMethod = false;
		ArrayList<String> annos = null;
		JMethod jm = null;
		while((line = next(false)) != null) {
			String lineRaw = line;
			line = line.trim();
			if(line.startsWith("//") || line.startsWith("/*")) {
				if(isInMethod) {//方法体内部的注释
					jm.appendBodyLine(lineRaw);
				} else {
					topDesc = JDesc.parse(this, lineRaw);//方法上面，或者成员变量上面的注释
				}
			} else if(line.startsWith("@")) {
				if(annos == null)annos = new ArrayList<>();
				annos.add(line);
				
			} else if(line.startsWith("public ") || line.startsWith("private ") || line.startsWith("protected ")) {
				String descTrimed = trimRightDesc(line);
				if(descTrimed.endsWith("{") || descTrimed.endsWith(",")) {
					appendRawMethod(jm, false);
					isInMethod = true;
					jm = new JMethod(this, line);
					jm.annoLines = annos;
					jm.descTop = topDesc;
				} else {
					isInMethod = false;
					JField jf = new JField(line);
					jf.annoLines = annos;
					jf.descTop = topDesc;
					fields.put(jf.name, jf);
				}
				annos = null;
				topDesc = null;
			} else {
				if(jm != null) {
					jm.appendBodyLine(lineRaw);
				}
				//待优化
				if(lineRaw.startsWith("    }")) {
					isInMethod = false;
				}
			}
		}
		appendRawMethod(jm, true);
	}

	private String trimRightDesc(String line) {
		String rt = line;
		int idx = rt.indexOf("//");
		if(idx>0) {
			rt = rt.substring(0, idx).trim();
		}
		idx = rt.indexOf("/*");
		if(idx>0) {
			rt = rt.substring(0, idx).trim();
		}
		return rt;
	}

	private void parseDefLine(String line) {
		Matcher matcher = RE.findMatcher(line);
		if(matcher != null) {
			if(matcher.group(1) != null) {
				isStatic = true;
			}
			if(matcher.group(2) != null) {
				isFinal = true;
			}
			name = matcher.group(3);
		}
	}
	
	private void appendRawMethod(JMethod jm, boolean last) {
		if(jm != null) {
			if(last) {
				jm.popBodyLine();
			}
			jm.popBodyLine();
			addMethod(jm);
		}
	}
	
	public String next() throws IOException {
		String line = null;
		do {
			line = mBr.readLine();
			if(line == null) {
				break;
			}
			line = line.trim();
		} while(line.length() == 0);
		return line;
	}
	
	public String next(boolean trim) throws IOException {
		String line = null;
		String trimed = null;
		do {
			line = mBr.readLine();
			if(line == null) {
				break;
			}
			trimed = line.trim();
		} while(trimed.length() == 0);
		return trim ? trimed : line;
	}
	
}
