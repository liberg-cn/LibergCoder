package cn.liberg.coder.tool.java;

/**
 * 将 {@link MetaAnno} 解析为 {@code cn.liberg.annotation.cache} 注解相关属性
 */
public class CacheAnno {
    public static final String KEY = "cache";

    public String cap = "0";
    public String group = "";
    public String groupCap = "TenThousands.X1";
    public int seq = 0;

    public static CacheAnno from(MetaAnno anno) {
        CacheAnno obj = new CacheAnno();
        obj.cap = anno.getValue(KEY, "cap", "0");

        String group = anno.getValue(KEY, "group");
        if(group != null) {
            obj.group = group;
            obj.groupCap = anno.getValue(KEY, "groupCap", "TenThousands.X1");
            String seq = anno.getValue(KEY, "seq");
            if(seq != null) {
                obj.seq = Integer.parseInt(seq);
            }
        }
        return obj;
    }

    public boolean capGt0() {
        return !"0".equals(cap);
    }
}
