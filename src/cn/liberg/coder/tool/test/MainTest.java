package cn.liberg.coder.tool.test;

import cn.liberg.coder.tool.Config;
import cn.liberg.coder.tool.LibergToolContext;
import cn.liberg.coder.tool.java.JClassEntity;
import cn.liberg.coder.tool.template.TempDaoImpl;

public class MainTest {

    public static LibergToolContext getContext() throws Exception {
        return new LibergToolContext(
                "liberg-demo",
                "D:\\dev\\java\\github\\liberg-demo",
                "cn.liberg.demo",
                Config.defaultConfig
        );
    }

    public static void testDaoImpl(LibergToolContext context, JClassEntity entity) throws Exception {
        TempDaoImpl daoImpl = new TempDaoImpl(context, entity);

        daoImpl.save();
        System.out.println("End...");
    }

    public static void main(String[] args) throws Exception {
        LibergToolContext context = getContext();

        String entityName = "User";
        JClassEntity entity = new JClassEntity(context, context.getEntityPath() + entityName +".java");
        if (entity.name == null) {
            entity.name = entityName;
        }
        testDaoImpl(context, entity);



    }


}
