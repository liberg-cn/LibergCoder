package cn.liberg.coder.intellij;

import cn.liberg.coder.tool.LibergTool;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.Presentation;
import com.intellij.openapi.ui.Messages;

public class AboutAction extends AnAction {

    @Override
    public void actionPerformed(AnActionEvent event) {
        String msg = "version: " + LibergTool.getVersion();
        msg += "\r\n- https://gitee.com/liberg-cn/Liberg";
        msg += "\r\n- https://github.com/liberg-cn/Liberg";
        Messages.showMessageDialog(msg, "LibergCoder", Messages.getInformationIcon());
    }

    @Override
    public void update(AnActionEvent e) {
        super.update(e);
        Presentation presentation = e.getPresentation();
        presentation.setText("v"+LibergTool.getVersion());
    }

}
